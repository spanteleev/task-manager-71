package ru.tsc.panteleev.tm.api.service;

public interface ITokenService {

    String getToken();

    void setToken(String token);

}
