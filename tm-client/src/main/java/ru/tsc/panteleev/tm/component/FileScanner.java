package ru.tsc.panteleev.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.event.ConsoleEvent;
import ru.tsc.panteleev.tm.listener.AbstractListener;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    public Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    private final File folder = new File("./");

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean isCommand = listeners.contains(fileName);
            try {
                if (isCommand) {
                    file.delete();
                    applicationEventPublisher.publishEvent(new ConsoleEvent(fileName));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

}
