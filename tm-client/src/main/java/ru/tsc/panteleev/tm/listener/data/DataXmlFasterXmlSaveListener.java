package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataXmlFasterXmlSaveRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataXmlFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    private static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlFasterXmlSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlFasterXmlSaveRequest(getToken()));
    }

}
