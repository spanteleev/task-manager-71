package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataYamlFasterXmlSaveRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataYamlFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in yaxml file";

    @NotNull
    private static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlFasterXmlSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlFasterXmlSaveRequest(getToken()));
    }

}
