package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;

import java.util.List;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    @Nullable
    ProjectDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT p FROM ProjectDto p WHERE userId = :userId ORDER BY :sortColumn")
    List<ProjectDto> findAllByUserIdSort(@Nullable @Param("userId") String userId,
                                         @Nullable @Param("sortColumn") String sortColumn);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}
