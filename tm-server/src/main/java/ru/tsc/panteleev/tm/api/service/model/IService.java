package ru.tsc.panteleev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    void set(@NotNull Collection<M> models);

    @Nullable
    List<M> findAll();

}
