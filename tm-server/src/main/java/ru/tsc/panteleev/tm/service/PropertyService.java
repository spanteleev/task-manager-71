package ru.tsc.panteleev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @NotNull
    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    public String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    public Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUserName;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databaseUserPassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.sql_dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2dll_auto']}")
    private String databaseHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheConfigFile;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheFactoryClass;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

}
