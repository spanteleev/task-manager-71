package ru.tsc.panteleev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.model.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.model.ITaskService;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.Task;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    protected ITaskRepository repository;

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description,
                       @Nullable final Date dateBegin,
                       @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUser(getUserRepository().findById(userId).orElse(null));
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task changeStatusById(@Nullable final String userId,
                                 @Nullable String id,
                                 @Nullable Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @NotNull final Task task = findById(userId, id);
        task.setStatus(status);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull String userId) {
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull String userId, @Nullable Sort sort) {
        if (sort == null) return findAll(userId);
        return repository.findAllByUserIdSort(userId, sort.getOrderColumn());
    }

    @NotNull
    @Override
    public Task findById(@NotNull String userId, @NotNull String id) {
        Task task = repository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    @Transactional
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = repository.findByUserIdAndId(userId, id);
        if (task == null) return;
        repository.delete(findById(userId, id));
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        repository.deleteAll(findAll(userId));
    }

    @Override
    public long getSize(@NotNull String userId) {
        return repository.countByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<Task> tasks) {
        repository.saveAll(tasks);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    public void update(@NotNull Task task) {
        repository.saveAndFlush(task);
    }

    @Override
    @Transactional
    public void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        repository.deleteAll(findAllByProjectId(userId,projectId));
    }

}
