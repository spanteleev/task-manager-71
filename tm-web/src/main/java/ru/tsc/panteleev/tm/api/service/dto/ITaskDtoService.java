package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoService {

    TaskDto create(@NotNull final String userId);

    boolean existsById(@NotNull final String userId, @NotNull String id);

    @Nullable
    TaskDto findById(@NotNull final String userId, @NotNull String id);

    @Nullable
    List<TaskDto> findAll(@NotNull final String userId);

    long count(@NotNull final String userId);

    void save(@NotNull final String userId, @NotNull TaskDto task);

    void deleteById(@NotNull final String userId, @NotNull String id);

    void delete(@NotNull final String userId, @NotNull TaskDto task);

    void deleteAll(@NotNull final String userId, @NotNull List<TaskDto> taskList);

    void clear(@NotNull final String userId);

}
