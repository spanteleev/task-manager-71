package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.api.endpoint.IProjectDtoEndpoint;
import ru.tsc.panteleev.tm.api.service.dto.IProjectDtoService;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.IProjectDtoEndpoint")
public class ProjectEndpointImpl implements IProjectDtoEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public ProjectDto create() {
        return projectDtoService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectDtoService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectDtoService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectDtoService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectDtoService.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectDtoService.save(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final ProjectDto project
    ) {
        projectDtoService.delete(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        projectDtoService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody final List<ProjectDto> projects
    ) {
        projectDtoService.deleteAll(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectDtoService.clear(UserUtil.getUserId());
    }

}

