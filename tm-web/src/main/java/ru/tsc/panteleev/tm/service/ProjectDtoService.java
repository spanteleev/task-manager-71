package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.panteleev.tm.api.service.dto.IProjectDtoService;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @Modifying
    @Transactional
    public void add(@Nullable final String userId, @Nullable final ProjectDto project) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    @Modifying
    @Transactional
    public ProjectDto create(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName("New Project " + System.currentTimeMillis() / 1000);
        project.setDescription(UUID.randomUUID().toString());
        projectRepository.saveAndFlush(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(ProjectIdEmptyException::new);
        return projectRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public ProjectDto findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(ProjectIdEmptyException::new);
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<ProjectDto> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return projectRepository.countByUserId(userId);
    }

    @Override
    @Modifying
    @Transactional
    public void save(@Nullable final String userId, @Nullable final ProjectDto project) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    @Modifying
    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(ProjectIdEmptyException::new);
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Modifying
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final ProjectDto project) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        deleteById(userId, project.getId());
    }

    @Override
    @Modifying
    @Transactional
    public void deleteAll(@Nullable final String userId, @Nullable final List<ProjectDto> projectList) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectList).orElseThrow(ProjectNotFoundException::new);
        projectList.forEach(project -> deleteById(userId, project.getId()));
    }

    @Override
    @Modifying
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        projectRepository.deleteByUserId(userId);
    }

}
