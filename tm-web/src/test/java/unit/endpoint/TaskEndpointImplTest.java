package unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.panteleev.tm.configuration.ApplicationConfiguration;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskEndpointImplTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final TaskDto task1 = new TaskDto();

    @NotNull
    private final TaskDto task2 = new TaskDto();

    @NotNull
    private final TaskDto task3 = new TaskDto();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("testWeb", "testWeb");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(task1);
        save(task2);
        save(task3);
    }

    @After
    public void clean() {
        clear();
    }

    @SneakyThrows
    private void save(@NotNull final TaskDto taskDto) {
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void saveTest() {
        @NotNull final TaskDto task = new TaskDto();
        save(task);
        Assert.assertNotNull(findById(task.getId()));
    }

    @SneakyThrows
    public long count() {
        @NotNull final String countUrl = TASK_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class);
    }

    @Test
    public void countTest() {
        Assert.assertEquals(3, count());
    }

    @SneakyThrows
    public Boolean existsById() {
        @NotNull String url = TASK_URL + "existsById/" + task1.getId();
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return false;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertEquals(true, existsById());
    }

    @Nullable
    @SneakyThrows
    private TaskDto findById(@NotNull final String id) {
        @NotNull String url = TASK_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDto.class);
    }

    @Test
    public void findByIdTest() {
        @Nullable final TaskDto task = findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task.getId());
    }

    @NotNull
    @SneakyThrows
    private List<TaskDto> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDto[].class));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<TaskDto> taskList = findAll();
        Assert.assertEquals(3, taskList.size());
    }

    @SneakyThrows
    public void delete() {
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(task1);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteTest() {
        Assert.assertEquals(3, count());
        delete();
        Assert.assertNull(findById(task1.getId()));
        Assert.assertEquals(2, count());
    }

    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = TASK_URL + "deleteById/" + task1.getId();
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertEquals(3, count());
        deleteById();
        Assert.assertNull(findById(task1.getId()));
        Assert.assertEquals(2, count());
    }

    @SneakyThrows
    public void deleteAll() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task1);
        tasks.add(task2);
        @NotNull final String url = TASK_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(tasks);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAllTest() {
        Assert.assertEquals(3, count());
        deleteAll();
        Assert.assertNotNull(findById(task3.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    private void clear() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(3, count());
        clear();
        Assert.assertEquals(0, count());
    }

}
